var express = require('express');
var fs = require('fs');
var url = require('url');
var http = require('http');
var request = require('request');
var moment = require('moment')
var Q = require('q');
var AzureMedia = require('azure-media');
var azureStorage = require('azure-storage');
var myconfig = {
	client_id: 'videostorage', 
	client_secret: 'JCztkAWLzuUE1Gk1mzqHctUKkgvTMnCEp7WbBTgeCAw='
} 
//var verymodel = require('../verymodel');
var Busboy = require('busboy');
var app = express();

var AZURE_STORAGE_ACCOUNT = 'mediasvcgqvt9ln5t60dk',
	AZURE_STORAGE_ACCESS_KEY = 'tFOF5SYFHBdDvor5c/8OIEX4ANT8ODZST88qq51EFIsgHZmRdGKI5cy0/IoJwlm9koVSla2DI8HhcujFz/qmHg==';

app.use(express.static('./app'));

var api = new AzureMedia(myconfig); 
api.init(function (err) {
    console.log('Error: ', err);
});

var blobSvc = azureStorage.createBlobService(AZURE_STORAGE_ACCOUNT, AZURE_STORAGE_ACCESS_KEY);

app.get('/', function (req, res) {
	res.sendfile('app/views/index.html');
});

app.get('/getlist', function(req, res){
	var assetPromises = []
	api.rest.asset.list(function (err, assetlist) {
		if(err){
			console.log(err)
		}else{
			assetlist.forEach(function(asset){
				assetPromises.push(Q.denodeify(api.rest.asset.listParentAssets)(asset.Id))
				assetPromises.push(Q.denodeify(api.rest.asset.listLocators)(asset.Id))
			})
			Q.all(assetPromises).then(function(result){
				var assets = []
				for(i = 0; i < result.length; i+=2){
					var manifest = result[i][0].Name.split('.')
					manifest.pop()
					var url = result[i+1][0]!=null?
					result[i+1][0].Path + manifest.join('.') + '_H264_400kbps_AAC_und_ch2_96kbps.mp4'
					:'Not available'
					// var manifest = result[i][0].Name.split('.')
					// manifest[manifest.length-1] = 'ism'
					// var url = result[i+1][0]!=null?
					// result[i+1][0].Path + manifest.join('.') + '/Manifest'
					// :'Not available'
					var newAsset = {title: result[i][0].Name, url: url, showlink: result[i+1][0]!=null}
					console.log('New asset: ', newAsset)
					assets.push(newAsset)
				}
				res.statusCode = 200;
				res.send(assets);
				console.log('List of assets', assets.length, assets[0].data.toJSON().Id)
			})

		}
	}, {'$filter': "startswith(Name, 'JobOutputAsset')"})
})
app.get('/getstatus', function(req, res){
	console.log('Get the job status: ', req.query.id)

	var downloadUrl = 'Not yet available',
		status = '';

	Q.denodeify(api.rest.job.get)(req.query.id).then(function(response){
		console.log('State: ', response.State);
		status = response.State
		console.log('Just assigned state: ', status)
		if(response.State > 2){

			api.rest.job.listOutputMediaAssets(req.query.id, function (err, assets){

				encodedAsset = assets[0].toJSON()

				// api.rest.asset.listParentAssets(encodedAsset.Id, function(err, parentAsset){
				// 	console.log('Parent asset of encoded one: ', parentAsset.toJSON())
				// })

				Q.denodeify(api.rest.accesspolicy.findOrCreate)(43200.0, 1)
				.then(function(policy){
					console.log('Stream policy: ', policy.toJSON())
					return Q.denodeify(api.rest.locator.create)({
						AccessPolicyId: policy.Id, 
						AssetId: encodedAsset.Id, 
						StartTime: moment.utc().subtract(10, 'minutes').format('M/D/YYYY hh:mm:ss A'), 
						Type: 2});
				})
				.then(function(locator){
					console.log('Stream locator: ', locator.toJSON())
					api.rest.asset.listFiles(encodedAsset.Id, function(err, files){
						var filename = files[0].toJSON().Name.split('.')
						filename.pop()
						filename = filename.join('.') + '_H264_400kbps_AAC_und_ch2_96kbps.mp4'
						downloadUrl = locator.Path + filename
						res.statusCode = 200
						res.send({url: downloadUrl, status: status})
					}, {'$filter': "MimeType eq 'application/octet-stream'"})
				})
			})
		}


	})
})
app.post('/add',function(req,res){
	var busboy = new Busboy({ headers: req.headers });
	console.log('Request: ', req.body)
	var promise = {},
		newAsset = {},
		jobId = '',
		downloadUrl = '',
		assetName = '',
		filePath;

	busboy.on('field', function(fieldname, value){
		assetName = value
	})

    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
    	//console.log(file);
  //   	filePath = __dirname + '/public/' + filename; 
		// file.pipe(fs.createWriteStream(filePath));

		// //var uploadStream
		var size
		file.on('data', function(data){
			//uploadStream += data
			size += data.length
			console.log('got %d bytes of data', data.length);
		})
		file.on('end', function() {

		//console.log(uploadStream)

		promise = Q.denodeify(api.rest.asset.create)({Name: assetName})
		.then(function(asset){
			console.log("Created asset: " + asset.Id);
			newAsset = asset;

			var data = {
				"Name":"AssetDeliveryPolicy",
				"AssetDeliveryProtocol":7,
				"AssetDeliveryPolicyType":2,
				"AssetDeliveryConfiguration":null
			}
			var headers = api.defaultHeaders()
			request.post({
	            uri: 'https://wamsamsclus001rest-hs.cloudapp.net/api/AssetDeliveryPolicies',
	            headers: headers,
	            body: JSON.stringify(data),
	            followRedirect: false,
	            strictSSL: true
	        }, function (err, res) {
	            if (res.statusCode == 201) {
	                var data = JSON.parse(res.body).d;
	            } else {
	               console.log(err + res.statusCode + '\n' + res.body);
	            }
	        });

			return Q.denodeify(api.rest.assetfile.create)({Name: filename, ParentAssetId: asset.Id});
		})
		.then(function(assetfile){
			console.log("Created assetfile: " + assetfile.Id);
			return Q.denodeify(api.rest.accesspolicy.findOrCreate)(5, 2);
		})
		.then(function(policy){
			console.log("Created policy: " + policy.Id);
			return Q.denodeify(api.rest.locator.create)({
				AccessPolicyId: policy.Id, 
				AssetId: newAsset.Id, 
				StartTime: moment.utc().subtract(10, 'minutes').format('M/D/YYYY hh:mm:ss A'), 
				Type: 1,
				Name: filename});
		})
		.then(function(locator){
			console.log("Created locator: " + locator.Id);
			var locatorPath = url.parse(locator.BaseUri).pathname.substring(1);

			// var stats = fs.statSync(filePath)
			// var fileSizeInBytes = stats["size"]

			// var fileStream = fs.createReadStream(filePath)

			var deferred = Q.defer();
			//blobSvc.createBlockBlobFromStream(locatorPath, filename, fileStream, fileSizeInBytes
			blobSvc.createBlockBlobFromStream(locatorPath, filename, file, size
			, function(error, result, response){
				if(!error){
					deferred.resolve(result);
				}
				else{
					deferred.reject(error);
				}
			})
			return deferred.promise;
		})

		console.log('File [' + fieldname + '] Finished ');
	});

    });
    busboy.on('finish', function() {
    	promise.then(function(response){
			console.log('File uploaded successfully: ', response);

			// fs.unlink(filePath, function(err){
			// 	if(err){
			// 		console.log('Error during delete')
			// 	}else{
			// 		console.log('Successfully deleted: ', filePath)
			// 	}
			// })

    		api.media.encodeVideo(newAsset.Id, 'H264 Adaptive Bitrate MP4 Set SD 4x3', function (err, job, asset) {
				res.statusCode = 200;
	      		res.send({asset: {title: newAsset.Name, url: 'Encoding...', showlink: false}, job: job.toJSON().Id});
      		})
		}).catch(function(error){
			console.log('Error occured: ', error)
		});
    });

    return req.pipe(busboy);
});

app.delete('/deleteall', function(response){
    api.rest.accesspolicy.list(function (err, policies) {
        policies.forEach(function (policy) {
            console.log(policy.Id);
            api.rest.accesspolicy.delete(policy.Id);
        });
        api.rest.asset.list(function (err, assets) {
            assets.forEach(function (asset) {
                console.log(asset.Id);
                asset.delete();
            });
            api.rest.assetfile.list(function (err, files) {
                files.forEach(function (file) {
                    console.log(file.Id);
                    if(file != undefined) {
                    	api.rest.assetfile.delete(file.Id)
                	}
                });
            });
        });
    });
})

var port = process.env.OPENSHIFT_NODEJS_PORT || process.env.npm_package_config_port || 10000,
	ipaddress = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

var server = app.listen(port, ipaddress, function () {
var host = server.address().address;
var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});