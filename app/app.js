var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', '$sceDelegateProvider',
  function($routeProvider,$sceDelegateProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
    $sceDelegateProvider.resourceUrlWhitelist([
      // Allow same origin resource loads.
      'self',
      // Allow loading from our assets domain.  Notice the difference between * and **.
      'http://videostorage.streaming.mediaservices.windows.net/**'
    ])
  }])

app.controller('MainCtrl', function($scope, uploadService) {
  uploadService.getVideos().then(function(response){
    $scope.videos = response.data
  })
})

app.factory('uploadService', function($http, $log, $q) {
  return {
   uploadVideo: function(data, tag) {
      var fd = new FormData();
      fd.append("file", data);
      fd.append("tag", tag)

     var deferred = $q.defer();
     $http.post('/add', fd, { withCredentials: true, headers: {'Content-Type': undefined }, transformRequest: angular.identity})
       .success(function(data, status) { 
          deferred.resolve({
             data: data,
             status: status});
       }).error(function(data, status) {
          deferred.reject(data);
          $log.error(data, status);
       });
     return deferred.promise;
   },
   getVideos: function(){
      var deferred = $q.defer();
      $http.get('/getlist')
      .success(function(data, status) { 
          deferred.resolve({
             data: data,
             status: status});
       }).error(function(data, status) {
          deferred.reject(data);
          $log.error(data, status);
       });
     return deferred.promise;
   },
   getEncodingStatus: function(jobId){
      var deferred = $q.defer();
      console.log('Job id passing to get status: ', jobId)
      $http.get('/getstatus', { params : {id: jobId}})
      .success(function(data, status) { 
          console.log('Got the following status: ' +  data.Url + '  ' + data.Status + '  ' + status + ' ' + jobId)
          deferred.resolve({
             data: data,
             status: status});
       }).error(function(data, status) {
          deferred.reject(data);
          $log.error(data, status);
       });
     return deferred.promise;
   }
  }
 });

app.directive('atDragNDrop', function($compile, $http, $q, $log, $interval, uploadService) {
  return {
    restrict: 'A',
    scope: true,
    controller: function($scope) {
        var stop
        $scope.deleteall = function(){
          var deferred = $q.defer();
          $http.delete('/deleteall')
          .success(function(data, status) { 
              deferred.resolve({
                 data: data,
                 status: status});
           }).error(function(data, status) {
              deferred.reject(data);
              $log.error(data, status);
           });
           return deferred.promise;
        }
        $scope.upload = function(file, tag){
          uploadService.uploadVideo(file, tag).then(function(response){
            response.data.asset.showlink = false;
            $scope.videos.push(response.data.asset)
            $scope.pollingStatus(response.data.job)
          })
        }
        $scope.stopPolling = function(){
          if(angular.isDefined(stop)){
            var success = $interval.cancel(stop)
            stop = undefined
            console.log('Stopped polling: ' + success + ' - ' + stop)
          }
        }
        $scope.pollingStatus = function(jobId){
          if(angular.isDefined(stop)) return;
          stop = $interval(function(){
            if(stop != undefined){
              uploadService.getEncodingStatus(jobId).then(function(data){
                console.log('Handled response: ', data.data, stop)
                if(data.data.status > 2){
                  console.log('Stopping status call: ', stop)
                  $scope.videos[$scope.videos.length-1].showlink = true
                  $scope.videos[$scope.videos.length-1].url = data.data.url
                  $scope.stopPolling()
                }
              })
           }
          }, 20000)
        }
        $scope.$on('$destroy', function(){
          $scope.stopPolling()
        })
    },
    link: function(scope, elem, attrs) {
      // add the dataTransfer property for use with the native `drop` event
      // to capture information about files dropped into the browser window
      jQuery.event.props.push( "dataTransfer" ); 

      var modal = "<div class='modal fade at-tag-file-modal-sm' tabindex='-1' role='dialog' aria-labelledby='mySmallModalLabel'><div class='modal-dialog modal-sm'><div class='modal-content form-inline'><div class='modal-header'>Tag the video</div><div class='modal-body'><input text='{{tag}}' class='form-control' style='width: 60%;margin-right: 15px;' ng-model='tag'/><button type='submit' class='btn btn-default btn-md' ng-click='upload(file, tag)' data-dismiss='modal'><span class='glyphicon glyphicon-upload'></span> Upload</button></div></div></div></div>"

      elem.on('drop', function(e) {
        e.preventDefault();
        e.stopPropagation();

        elem.find('.modal').remove()

        if(!e.dataTransfer.files[0].type.startsWith('video') || !e.dataTransfer.files.length > 1){
          $('.at-unsupported-file-modal-sm').modal({keyboard: true})
          return;
        }

        scope.file = e.dataTransfer.files[0];
        scope.tag = scope.file.name

        var compiledModal = $compile(modal)(scope)
        scope.$apply()
        elem.append(compiledModal)
        elem.find('.modal').on('keypress', function(e){
          if (e.keyCode == 13) {
            $('.at-tag-file-modal-sm').find('button').click()
          }
        })

        $('.at-tag-file-modal-sm').modal({keyboard: true})
      });
      elem.on('dragover', function(e) {
        e.preventDefault();
      })
    }
  }
})